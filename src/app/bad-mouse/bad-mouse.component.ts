import { Component } from '@angular/core';

@Component({
  selector: 'app-bad-mouse',
  templateUrl: './bad-mouse.component.html',
  styleUrls: ['./bad-mouse.component.scss']
})
export class BadMouseComponent {

  // This should be initialized in the constructor, not here!
  color = 'red';
  width = 500;
  height = 500;

  startX = 0;
  startY = 0;

  endX = 0;
  endY = 0;

  constructor() { }

  onMouseDown(event: MouseEvent) {

    this.startX = event.offsetX;
    this.startY = event.offsetY;
    console.log(`Mouse Down on x: ${this.startX} y: ${this.startY}`);
  }

  onMouseUp(event: MouseEvent) {

    this.endX = event.offsetX;
    this.endY = event.offsetY;
    console.log(`Mouse Up on x: ${this.endX} y: ${this.endY}`);

    console.log(`Total distance : ${this.calculateDistance()}`);
  }

  // Strongly coupled with the rest of the component
  // We need to set 4 different variables or call 2 different functions to make this work
  calculateDistance(): number {
    const distanceX = Math.abs(this.endX - this.startX);
    const distanceY = Math.abs(this.endY - this.startY);

    const totalDistance = Math.sqrt(Math.pow(distanceX, 2) + Math.pow(distanceY, 2));

    return totalDistance;
  }

  /**
   * Returns the euclidian distance between two points.
   * @param startX X coordinate of the first point.
   * @param startY Y coordinate of the first point.
   * @param endX X coordinate of the last point.
   * @param endY Y coordinate of the last point.
   * @returns Absolute value of the distance between the two points
   * @todo Point coordinates should be in an object instead of separate values
   */
  calculateDistancePure(startX: number, startY: number, endX: number, endY: number): number {
    const distanceX = Math.abs(endX - startX);
    const distanceY = Math.abs(endY - startY);

    const totalDistance = Math.sqrt(Math.pow(distanceX, 2) + Math.pow(distanceY, 2));

    return totalDistance;
  }
}
