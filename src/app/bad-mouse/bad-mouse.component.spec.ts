import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { BadMouseComponent } from './bad-mouse.component';

describe('BadMouseComponent', () => {
  let component: BadMouseComponent;
  let fixture: ComponentFixture<BadMouseComponent>;
  const LEFT_MOUSE_BUTTON_ID = 0;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [BadMouseComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BadMouseComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('onMouseDown should read the mouse pointer position', () => {
    const position = 100;
    const mockClick = new MouseEvent('mousedown', {
      button: LEFT_MOUSE_BUTTON_ID,
      clientX: position,
      clientY: position,
    });

    component.onMouseDown(mockClick);
    expect(component.startX).toBe(position);
    expect(component.startY).toBe(position);
  });

  it('onMouseUp should read the mouse pointer position', () => {
    const position = 100;
    const mockClick = new MouseEvent('mouseup', {
      button: LEFT_MOUSE_BUTTON_ID,
      clientX: position,
      clientY: position,
    });

    component.onMouseUp(mockClick);
    expect(component.endX).toBe(position);
    expect(component.endY).toBe(position);
  });

  it('should calculate distance between two points (THE BAD WAY : TOO MUCH REQUIREMENTS)', () => {
    const startPosition = { x: 0, y: 0 };
    const endPosition = { x: 3, y: 4 };
    const distance = 5;

    const mockClickDown = new MouseEvent('mousedown', {
      button: LEFT_MOUSE_BUTTON_ID,
      clientX: startPosition.x,
      clientY: startPosition.y,
    });
    component.onMouseDown(mockClickDown);

    const mockClickUp = new MouseEvent('mouseup', {
      button: LEFT_MOUSE_BUTTON_ID,
      clientX: endPosition.x,
      clientY: endPosition.y,
    });
    component.onMouseUp(mockClickUp);

    // The result of this test is affected by the correctness of "onMouseDown" and "onMouseUp"
    expect(component.calculateDistance()).toBe(distance);

  });

  it('should calculate distance between two points (THE BETTER WAY)', () => {
    const startPosition = { x: 0, y: 0 };
    const endPosition = { x: 3, y: 4 };
    const distance = 5;

    // This no longer depends on other methods
    // This function can be tested even if "onMouseDown" and "onMouseUp" are not done yet
    const calculatedDistance = component.calculateDistancePure(startPosition.x, startPosition.y,
      endPosition.x, endPosition.y);
    expect(calculatedDistance).toBe(distance);

  });

  it('should always calculate a positive distance', () => {
    // This is starting to be a lot of duplicated code and can create errors if we copy paste in each test.
    // See the beforeEach() function in "mouse-handler.service.spec.ts" about how to avoid this.
    const startPosition = { x: 3, y: 4 };
    const endPosition = { x: 0, y: 0 };
    const distance = 5;

    const calculatedDistance = component.calculateDistancePure(
      startPosition.x, startPosition.y,
      endPosition.x, endPosition.y);
    expect(calculatedDistance).toBeGreaterThanOrEqual(0);
    expect(calculatedDistance).toBe(distance);

  });

});

