# Exemple de tests avec Angular

Ceci est un projet simple visant à démontrer comment écrire des tests pour des projets Angular avec la librairie `Jasmine`.
Pour plus de détails, consultez la documentation d'Angular qui va plus en profondeurs sur les stratégies de tests. Les tutoriels sont disponibles [ICI](https://angular.io/guide/testing). Consultez [la base des tests des Components](https://angular.io/guide/testing-components-basics) et [comment tests les Services](https://angular.io/guide/testing-services) pour plus d'informations.  

## Lancement du projet

Exécutez `npm start` pour un serveur de développement local. Aller à `http://localhost:4200/`. Le site rechargera automatiquement si vous modifiez le code source.

## Utilisation d'Angular CLI

Exécutez `ng generate component component-name` pour un nouveau component. Vous pouvez également utiliser `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Lancement des tests unitaires

Exécutez `npm test` pour exécuter les tests unitaires via [Karma](https://karma-runner.github.io).

## Couverture du code

Exécutez `npm run coverage` pour exécuter les tests unitaires et calculer la couverture du code.
Vous pouvez accéder à `index.html` dans le répertoire **coverage** pour explorer le rapport sur la couverture du code.

# Fichiers principaux du projet

Le projet contient 2 Components qui implémentent la même fonctionnalité : gérer les événements de la souris et calculer la distance entre l'événement d'appui et relâchement de la souris. Les informations sont imprimées dans la console.

## Bad Mouse Component

Implémente l'ensemble de la logique dans la même classe : le Component.
Ceci est un exemple des erreurs potentielles lorsqu'on mélange la logique d'affichage et la logique de traitement ainsi que la difficulté supplémentaire lors des tests.

**Note:** l'utilisation de seulement un Component sans Service n'est pas nécessairement une mauvaise pratique. Tout dépend de votre projet et vos besoins.

## Mouse Component and MouseHandler Service

Implémente la logique de traitement dans une classe séparée : un Service.
Ceci est un exemple des avantages de la division de la logique d'affichage et la logique de traitement ainsi que la manière de tester cette approche

Voir `mouse.component.spec.ts` pour un exemple de comment utiliser un `Jasmine Spy` comme un `Mock` pour replacer une dépendance lors d'un test.

Voir `mouse-handler.service.spec.ts` pour un exemple de comment utiliser un `Jasmine Spy` comme un `Spy` sur une vraie fonction.

# Exercices

Le but de l'exercice est d'expérimenter avec les différentes manières de communiquer entre Components disponibles avec Angular et leur impact au niveau de l'architecture du système ainsi que la manière de tester chaque approche.

## Nouvelle fonctionnalité 
Pour chaque exercice, l'objectif est d'implémenter une nouvelle fonctionnalité. Dans cette fonctionnalité, les données calculées par la gestion des événements de la souris (position de départ, de fin et position courante) ainsi que la distance entre les points de départ et fin sont affichées sur l'écran et non dans la Console. Pour l'ensemble des exercices, seulement le component `MouseComponent` sera considéré.

## Exercice 0

Implémenter la gestion de l'événement de déplacement de la souris [`mousemove`](https://developer.mozilla.org/en-US/docs/Web/API/Element/mousemove_event). La position de la souris est enregistrée dans `MouseHandlerService`.

## Exercice 1

Implémenter un nouveau Component qui permet d'afficher l'information pertinente (position de départ, position de fin, position courante et distance parcourue). Ce component devra être un enfant de `MouseComponent` et l'information lui est passée à travers des propriétés utilisant le décorateur `@Output()`.

## Exercice 2

Implémenter un nouveau Component qui permet d'afficher l'information pertinente (position de départ, position de fin, position courante et distance parcourue). Ce component récupère l'information pertinente à travers `MouseHandlerService` qui est une dépendance injectée dans le Component. Ce component n'a aucun lien de parenté direct avec `MouseComponent`.